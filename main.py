import sys

import numpy as np
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QMainWindow, QApplication, QSizePolicy
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.lines as mlines
import matplotlib.patches as mpatches

from fmath import Sin, Cos, Tan, Pow, Exp, Linear, Sum, Product
from fmath.interpolation import interpolate

from main_ui import Ui_MainWindow


class UI(QMainWindow, Ui_MainWindow):
    def __init__(self, ):
        self.min_max_height = (-10, 10)
        self.maxGraphicsRange = 100
        super(UI, self).__init__()
        self.setupUi(self)
        self.set_defaults()
        self.update_figure()
        self.bind_updaters()
        self.formulaLabel.setScaledContents(True)
        self.formulaLabel.setPixmap(QPixmap("formula.png"))

    def __init_graphic__(self, fig):
        self.canvas = FigureCanvas(fig)
        self.canvas.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))
        self.graphLayout.addWidget(self.canvas)
        self.canvas.draw()

    def __remove_graphic__(self):
        try:
            self.graphLayout.removeWidget(self.canvas)
            self.canvas.close()
        except Exception:
            print('No canvas widget bound to layout \
                  perhaps, it\'s first use of this function')

    def bind_updaters(self):
        self.updateButton.clicked.connect(self.update_figure)
        self.fxCheckBox.stateChanged.connect(self.update_figure)
        self.dfxCheckBox.stateChanged.connect(self.update_figure)
        self.pnxCheckBox.stateChanged.connect(self.update_figure)
        self.dpnxCheckBox.stateChanged.connect(self.update_figure)
        self.rnxCheckBox.stateChanged.connect(self.update_figure)
        self.alphaEdit.valueChanged.connect(self.update_figure)
        self.betaEdit.valueChanged.connect(self.update_figure)
        self.gammaEdit.valueChanged.connect(self.update_figure)
        self.epsilonEdit.valueChanged.connect(self.update_figure)
        self.nEdit.valueChanged.connect(self.update_figure)
        self.fitToHeight.clicked.connect(self.fit_to_height_graphic)

    def fit_to_height_graphic(self):
        self.cEdit.setValue(self.min_max_height[0])
        self.dEdit.setValue(self.min_max_height[1])
        self.update_figure()

    def redraw(self, figure):
        self.__init_graphic__(figure)

    def set_defaults(self):
        self.aEdit.setRange(-self.maxGraphicsRange, self.maxGraphicsRange)
        self.aEdit.setValue(-self.maxGraphicsRange / 10)
        self.bEdit.setRange(-self.maxGraphicsRange, self.maxGraphicsRange)
        self.bEdit.setValue(self.maxGraphicsRange / 10)
        self.cEdit.setRange(-self.maxGraphicsRange, self.maxGraphicsRange)
        self.cEdit.setValue(-self.maxGraphicsRange / 10)
        self.dEdit.setRange(-self.maxGraphicsRange, self.maxGraphicsRange)
        self.dEdit.setValue(self.maxGraphicsRange / 10)

        self.alphaEdit.setRange(-self.maxGraphicsRange, self.maxGraphicsRange)
        self.alphaEdit.setValue(1.0)
        self.betaEdit.setRange(-self.maxGraphicsRange, self.maxGraphicsRange)
        self.betaEdit.setValue(1.0)
        self.gammaEdit.setRange(-self.maxGraphicsRange, self.maxGraphicsRange)
        self.gammaEdit.setValue(1.0)
        self.epsilonEdit.setRange(-self.maxGraphicsRange, self.maxGraphicsRange)
        self.epsilonEdit.setValue(1.0)

        self.nEdit.setRange(1, 200)
        self.nEdit.setValue(1)

        for i in range(5):
            self.deltaEdit.addItem(str(10 ** (-i)), 10 ** (-i))

        self.fxCheckBox.setChecked(True)
        self.pnxCheckBox.setChecked(True)

    def form_data(self):
        left_x = float(self.aEdit.text())
        right_x = float(self.bEdit.text())
        bottom_y = float(self.cEdit.text())
        top_y = float(self.dEdit.text())

        a = float(self.alphaEdit.text())
        b = float(self.betaEdit.text())
        g = float(self.gammaEdit.text())
        e = float(self.epsilonEdit.text())

        # f = Cos(e, 1, Sum(Linear(b, a), Exp(1, g)))
        f = Product(Cos(a=a, b=1, arg=Tan(1, 1, Sum(Pow(b, 2), Linear(g)))), Sin(a=1, b=e, arg=None))
        # f = Cos(1, 1)
        x = np.linspace(left_x, right_x, 500)
        y = f.f(x)

        return x, y, f, [bottom_y, top_y]

    def setup_figure(self):
        n = int(self.nEdit.text())
        step = float(self.deltaEdit.currentText())

        fig = Figure(figsize=(9.01, 9.01))

        x, y, f, y_lim = self.form_data()
        self.min_max_height = (min(y) - 0.25, max(y) + 0.25)

        _, y_i = interpolate(x, f, n)
        _, d_y_i = interpolate(x + step, f, n)

        ax = fig.add_subplot(111)

        # ax.spines['left'].set_position('center')
        # ax.spines['bottom'].set_position('center')
        # ax.spines['right'].set_color('none')
        # ax.spines['top'].set_color('none')
        legends = []

        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')

        if self.fxCheckBox.isChecked():
            ax.plot(x, y, "r")
            legends.append(mlines.Line2D([], [], color="red", label="График функции"))
        if self.pnxCheckBox.isChecked():
            ax.plot(x, y_i, "b")
            legends.append(mlines.Line2D([], [], color="blue", label="График интерп. многочлена"))
        if self.rnxCheckBox.isChecked():
            ax.plot(x, abs(y - y_i), "g--")
            legends.append(mlines.Line2D([], [], color="green", linestyle="--", label="График разности"))
            legends.append(mpatches.Patch(color="white", label="Максимум разности равен {}".format(round(max(abs(y-y_i)), 2))))
        if self.dfxCheckBox.isChecked():
            ax.plot(x, (f.f(x + step) - y) / step, "r--")
            legends.append(mlines.Line2D([], [], color="red", linestyle="--", label="График производной фунции"))
        if self.dpnxCheckBox.isChecked():
            ax.plot(x, (d_y_i - y_i) / step, "b--")
            legends.append(
                mlines.Line2D([], [], color="blue", linestyle="--", label="График производной интерп. многочлена"))
        ax.legend(handles=legends)
        ax.set_ylim(y_lim)

        return fig

    def update_figure(self):
        self.__remove_graphic__()
        self.redraw(self.setup_figure())


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ui = UI()
    ui.show()
    sys.exit(app.exec_())
