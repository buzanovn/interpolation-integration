from numpy import power, ndarray


class BaseFunction:  # Base function implementation
    """
    Just a * f(b * x)

    Parameters:
    ----------
    :arg coefficient: float number that multiplies the values of given function by it
    :arg arg_mul:
    """

    def __init__(self, a=None, b=None, arg=None):
        self.a = a
        self.arg = arg
        self.b = b
        self.__function__ = None

    def __check_arg__(self):
        return isinstance(self.arg, BaseFunction) or isinstance(self.arg, Composition)

    def f(self, x):
        pass

    def der(self):
        pass


class Const(BaseFunction):
    """
    Just f(x) = C
    """

    def __init__(self, arg):
        super().__init__(arg=arg)

    def f(self, x):
        if isinstance(x, ndarray):
            return self.arg * x
        else:
            return self.arg

    def der(self):
        return Const(0)


class Pow(BaseFunction):
    """
    f(x) = a * x^b
    """

    def __init__(self, a, b, arg=None):
        super().__init__(a, b, arg)
        self.__function__ = power

    def f(self, x):
        if self.__check_arg__():
            return self.a * self.__function__(self.arg.f(x), self.b)
        else:
            return self.a * self.__function__(x, self.b)

    def der(self):
        if self.__check_arg__():
            return Pow(self.a * self.b * self.arg.der(), self.b - 1, self.arg)
        else:
            return Pow(self.a * self.b, self.b - 1)


class Composition:
    """
    f(x) = a(x) o b(x) o ... o z(x)
    """

    def __init__(self, *args):
        self.functions = args

    def f(self, x):
        pass

    def der(self):
        pass


class Sum(Composition):
    """
    f(x) = a(x) + b(x) + ... + z(x)
    """

    def __init__(self, *args):
        super().__init__(args)

    def f(self, x):
        result = 0
        for function in self.functions[0]:
            result += function.f(x)
        return result

class Product(Composition):
    """
    f(x) = a(x) * b(x) * ... * z(x)
    """

    def __init__(self, *args):
        super().__init__(args)

    def f(self, x):
        result = 1
        for function in self.functions[0]:
            result *= function.f(x)
        return result


class Exp(BaseFunction):
    """
    f(x) = a * b ^ x
    """

    def __init__(self, a=1, b=1, arg=None):
        super().__init__(a, b, arg)
        self.__function__ = power

    def f(self, x):
        if self.__check_arg__():
            pass
        else:
            return self.a * self.__function__(self.b, x)
