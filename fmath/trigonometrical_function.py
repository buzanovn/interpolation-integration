from numpy import sin, cos, tan

from fmath.base_function import BaseFunction


class Trigonometrical(BaseFunction):
    def __init__(self, a=1, b=1, arg=None):
        super().__init__(a, b, arg)
        self.__function__ = None
        self.__name__ = None

    def f(self, x):
        if self.__check_arg__():
            return self.a * self.__function__(self.arg.f(x))
        else:
            return self.a * self.__function__(self.b * x)

    def __add__(self, other):
        return

    def __str__(self):
        return '{} * {}({})'.format(self.a, self.__name__,
                                    str(self.arg) if self.__check_arg__() else '{} * x'.format(self.arg))


class Sin(Trigonometrical):
    def __init__(self, a, b, arg=None):
        super().__init__(a, b, arg)
        self.__function__ = sin
        self.__name__ = 'sin'

    def der(self):
        return Cos(self.a * self.b, self.arg)


class Cos(Trigonometrical):
    def __init__(self, a, b, arg=None):
        super().__init__(a, b, arg)
        self.__function__ = cos
        self.__name__ = 'cos'

    def __mul__(self, other):
        if isinstance(other, int) or isinstance(other, float):
            return Cos(self.a * other, self.b, self.arg)


class Tan(Trigonometrical):
    def __init__(self, a, b, arg=None):
        super().__init__(a, b, arg)
        self.__function__ = tan
        self.__name__ = 'tan'
