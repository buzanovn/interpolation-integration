import numpy as np

from fmath.function import Linear


def interpolate(x, fun, n, debug_mode=False):
    """ Using the second Newtons formula
    """
    h = (x[-1] - x[0]) / n
    q = Linear(1 / h, -x[-1] / h)
    x_inter = np.array([x[0] + i * h for i in range(0, n + 1)])
    y_inter = fun.f(x_inter)
    table = create_table(y_inter, n)
    table_diag = [table[i][-1] for i in range(0, n + 1)]
    functions_list = [1]

    if debug_mode:
        print("h = {}".format(h))
        print("q = {}".format(q))
        print("x_inter = {}".format(x_inter))
        print("y_inter = {}".format(y_inter))
        print("table = {}".format(table))
        print("table_diag = {}".format(table_diag))

    for i in range(1, n + 1):
        functions_list.append(functions_list[-1] * (q.f(x) + i - 1) / i)
    y = [table_diag[i] * functions_list[i] for i in range(0, n + 1)]
    return x, (y[0] + sum(y[1:]))


def create_table(y_inter, n):
    table = []
    current_subtraction = np.array([y_inter[j] - y_inter[j - 1] for j in range(1, len(y_inter))])
    table.append(y_inter)
    table.append(current_subtraction)
    for i in range(2, n + 1):
        last = table[len(table) - 1]
        current_subtraction = np.array([last[j] - last[j - 1] for j in range(1, len(last))])
        table.append(current_subtraction)
    return table
