from fmath.base_function import BaseFunction, Const


class Linear(BaseFunction):
    """
    f(x) = ax + b
    """
    def __init__(self, a=1, b=0):
        super().__init__(a, b)

    def f(self, x):
        return self.a * x + self.b

    def der(self):
        return Const(self.a)

    def __add__(self, other):
        return Linear(self.a + other.a, self.b + other.b)

    def __str__(self):
        return '{} * x + ({})'.format(self.a, self.b)
