"""
The module documentation
"""
from fmath.base_function import Composition, Const, Exp, Pow, Sum, Product
from fmath.function import Linear
from fmath.trigonometrical_function import Cos, Sin, Tan
