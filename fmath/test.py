import unittest

import numpy as np

from fmath.base_function import Const
from fmath.trigonometrical_function import Sin


class TestFunctionMethods(unittest.TestCase):
    def test_arg(self):
        self.assertEqual(Sin(1, 2).__check_arg__(), False)
        self.assertEqual(Sin(1, Sin(1, 2)).__check_arg__(), True)

    def test_f(self):
        self.assertEqual(Const(1).f(2), 1)
        self.assertEqual(np.array_equal(Const(4).f(np.linspace(-1, 1, 20)), 4 * np.linspace(-1, 1, 20)), True)